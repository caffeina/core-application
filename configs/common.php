<?php
// Common config
return [
  'debug' => false,
  'cache' => [
    'dir' => APP_DIR.'/cache',
  ],
];